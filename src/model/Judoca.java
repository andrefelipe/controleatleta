package model;

public class Judoca extends Atleta{
	
	private String faixa;
	private String categoria;
	private String golpeForte;
	private boolean bomSolo;
	
	public Judoca (String nome, String faixa){
		super(nome);
		this.faixa = faixa;
	}

	public String getFaixa() {
		return faixa;
	}

	public void setFaixa(String faixa) {
		this.faixa = faixa;
	}

	public String getCategoria() {
		return categoria;
	}

	public void setCategoria(String categoria) {
		this.categoria = categoria;
	}

	public String getGolpeForte() {
		return golpeForte;
	}

	public void setGolpeForte(String golpeForte) {
		this.golpeForte = golpeForte;
	}

	public boolean isBomSolo() {
		return bomSolo;
	}

	public void setBomSolo(boolean bomSolo) {
		this.bomSolo = bomSolo;
	}
	
	
}
