package model;

public class Futebolista extends Atleta{
	
	private String posicao;
	private boolean destro;
	
	
	
	
	
	public Futebolista(String nome, String posicao) {
		super(nome);
		this.posicao = posicao;
	}
	public String getPosicao() {
		return posicao;
	}
	public void setPosicao(String posicao) {
		this.posicao = posicao;
	}
	public boolean isDestro() {
		return destro;
	}
	public void setDestro(boolean destro) {
		this.destro = destro;
	}

	
}
